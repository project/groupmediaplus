# GroupMediaPlus

* The groupmediaplus module provides a views argulent default to use in media
helper views.
* The groupmediaplus_upload module auto attaches media items created through
media helpers.

## Supported media helpers

* core media_library entity reference widget
* entity_browser entity reference widget
* entity_embed ckeditor button

## Supported parent entity group info

* Creating group content via group links like /group/1/node/create/article
* Editing existing group content
* Currently NOT: Setting the group via the upcoming pseudofield

## Gotchas

* Due to core limitations, the media_library integration does not work if the
referer header is not set
  (e.g. due to browser or site setting)

## Debugging

If something does not work as expected:

Switch debug messages on:
`drush cset groupmediaplus.settings debug 1`

Switch debug messages off:
`drush cdel groupmediaplus.settings debug`
